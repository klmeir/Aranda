# Aranda with .NET Core
## Build & Run

To startup the whole solution, execute the following command:

```
docker-compose build --no-cache
docker-compose up -d
```

[Go to http://localhost:8000](http://localhost:8000)